# Unity Dungeon #

Unityで風来のシレンのようなグリッドムーブメントを実現させるためのデモプログラムです。

### Unity Version ###
* Unity 5.5.0f3

### Controls ###
* 移動：矢印キー
* 方向転換：Shift
* 足踏み：スペースキー

### Downloads ###

* [Windows](https://bitbucket.org/gezanden/unitydungeon/get/dungeon/master.zip)
* [Source](https://bitbucket.org/gezanden/unitydungeon/get/test.zip)

# Unity Dungeon 2 #

Unity Dungeonに攻撃とアイテムボックスと武器の取得・装備などの機能を追加したものです。

### Controls ###
* 移動：矢印キー
* 方向転換：Shift
* 足踏み：スペースキー
* アイテムボックス：Esc
* 攻撃：Ctrl

### Downloads ###

* [Windows](https://bitbucket.org/gezanden/unitydungeon/get/dungeon2/master.zip)


## その他 ##
動作検証はWindows 10で行っています。  
GitクライアントはSourceTreeを使用しています。  
ウィルスチェックはAvira Command Line Scanner ScanCLにて検査しています。  
製作期間は共に約四ヶ月です。  
この制作に取りかかる前に、Unity公式にあるTutorialsを一通り行いました。